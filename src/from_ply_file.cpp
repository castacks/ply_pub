
#include <string>

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/io/ply_io.h>

template < typename PointT >
static typename pcl::PointCloud<PointT>::Ptr read_ply(
    const std::string& fn ) {
    typename pcl::PointCloud<PointT>::Ptr p_cloud(new pcl::PointCloud<PointT>);

    if ( -1 == pcl::io::loadPLYFile<PointT>(fn, *p_cloud) ) {
        std::stringstream ss;
        ss << "Failed to read " << fn << ". ";
        throw (std::runtime_error(ss.str()));
    }

    ROS_INFO("from_ply_file_node: read %d points from %s. ", 
        static_cast<int>(p_cloud->size()), fn.c_str());

    return p_cloud;
}

static std::string compose_param_name(
    const ros::NodeHandle& nh, 
    const std::string& param_name) {
    std::stringstream ss;
    ss << ros::this_node::getName() << "/" << param_name;
    return ss.str();
}

template < typename PCT >
static sensor_msgs::PointCloud2 pcl_2_ros(
    const PCT& pc,
    const std::string& frame_id ) {
    // Convert the point cloud into ROS message.
    sensor_msgs::PointCloud2 msgPC2;
    pcl::toROSMsg( pc, msgPC2 );

    // Configure the header.
    msgPC2.header.stamp = ros::Time::now();
    msgPC2.header.frame_id = frame_id;

    return msgPC2;
}

int main(int argc, char** argv) {    
    // The ROS node.
    ros::init(argc, argv, "from_ply_file_node");
    ros::NodeHandle nh;
    ros::Publisher pub = nh.advertise<sensor_msgs::PointCloud2>(
        "cloud_ply", 1, true);

    // Get the paramterers.
    std::string in_ply_fn;
    std::string frame_id;
    double interval;

    nh.getParam(compose_param_name(nh, "in_ply_fn"), in_ply_fn);
    nh.getParam(compose_param_name(nh, "frame_id"), frame_id);
    nh.getParam(compose_param_name(nh, "interval"), interval);

    ROS_INFO("in_ply_fn = %s", in_ply_fn.c_str());
    ROS_INFO("frame_id = %s", frame_id.c_str());
    ROS_INFO("interval = %f", interval);

    // Read the PLY file.
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr p_cloud = 
        read_ply<pcl::PointXYZRGB>( in_ply_fn );

    // Convert the point cloud into ROS message.
    sensor_msgs::PointCloud2 msgPC2 = pcl_2_ros( *p_cloud, frame_id );

    // Rate setting.
    ros::Rate loop_rate( 1. / interval );

    // Publish.
    while(nh.ok()) {
        pub.publish(msgPC2);
        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}
